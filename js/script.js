// 1. Метод forEach используется для итерации через элементы массива и выполнения определенного действия для каждого элемента.

// 2. Чтобы очистить массив можно использовать несколько способов: 

    // Установка свойства length массива на 0 приведет к его очистке. Например:
    // let array = [1, 2, 3, 4, 5];
    // array.length = 0;

    // Использование метода splice() с указанием индекса 0 и длины массива также приведет к его очистке:

    // let array = [1, 2, 3, 4, 5];
    // array.splice(0, array.length);

    // Присвоение нового пустого массива:
    // Просто присвоение пустого массива вместо текущего массива:

    // let array = [1, 2, 3, 4, 5];
    // array = [];

// 3. Можно проверить с помощью Оператор Array.isArray():

function filterBy(arr, type) {
  const dataType = typeof type === 'string' ? type : '';

  return arr.filter(item => {
    const itemType = typeof item;
    return itemType !== dataType;
  });
}

const arr = ['hello', 'world', 23, '23', null];
const filteredArr = filterBy(arr, 'string');
console.log(filteredArr);